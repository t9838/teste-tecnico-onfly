const createConnection = require('typeorm').createConnection;
const dotenv = require('dotenv');
const IndicatorTypeSeed = require('./indicatorType.seed').IndicatorTypeSeed;
const IndicatorSeed = require('./indicator.seed').IndicatorSeed;
const UserSeed = require('./user.seed').UserSeed;

dotenv.config();

createConnection({
    type: 'mariadb',
    host: process.env.DB_HOST,
    port: Number(process.env.DB_PORT),
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    logging: JSON.parse(process.env.DB_LOG),
}).then(async connection => {
    await new IndicatorTypeSeed(connection).execIndicatorTypeSeeds();
    await new IndicatorSeed(connection).execIndicatorSeeds();
    await new UserSeed(connection).execUserSeeds();
    process.exit(0);
}).catch(err => {
    console.log(err)
    process.exit(1);
})