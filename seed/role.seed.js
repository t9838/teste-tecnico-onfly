const dbConstants = require('../db.constants');

class RoleSeed {

    _connectionRef;

    constructor(connection){
        this._connectionRef = connection;
    }

    execRoleSeeds = async () => {
        await this._createAdminRole();
        await this._createClientRole();
    }
    
    _createAdminRole = async () => {

        await this._connectionRef.query(`
            INSERT IGNORE INTO indicator_entity (id, value, description, active, indicatorTypeId)
            VALUES (${dbConstants.indicators.role.ADMIN_ROLE}, 'Admin role', 'Role with admin privileges', 1, ${dbConstants.indicatorsTypes.ROLE})
        `);

        const AdminRoleAccessAndPermissionsQueries = [];

        for(const UserAccessAndPermissionsKey of Object.keys(dbConstants.indicators.user)){

            AdminRoleAccessAndPermissionsQueries.push(`
                (${dbConstants.indicators.role.ADMIN_ROLE}, ${dbConstants.indicators.user[UserAccessAndPermissionsKey]})
            `)
        };

        for(const SpendingAccessAndPermissionsKey of Object.keys(dbConstants.indicators.spending)){

            AdminRoleAccessAndPermissionsQueries.push(`
                (${dbConstants.indicators.role.ADMIN_ROLE}, ${dbConstants.indicators.spending[SpendingAccessAndPermissionsKey]})
            `)
        };

        await this._connectionRef.query(`
            INSERT IGNORE INTO
                roles_access_permissions_entity (roleEntityId, accessPermissionsEntityId)
            VALUES
                ${AdminRoleAccessAndPermissionsQueries.join(',')}
        `);

    }

    _createClientRole = async () => {

        await this._connectionRef.query(`
            INSERT IGNORE INTO indicator_entity (id, value, description, active, indicatorTypeId)
            VALUES (${dbConstants.indicators.role.CLIENT_ROLE}, 'Client role', 'Role with client privileges', 1, ${dbConstants.indicatorsTypes.ROLE})
        `);

        const ClientRoleAccessAndPermissionsQueries = [];

        ClientRoleAccessAndPermissionsQueries.push(`
            (${dbConstants.indicators.role.CLIENT_ROLE}, ${dbConstants.indicators.user.USER_ACCESS})
        `);

        ClientRoleAccessAndPermissionsQueries.push(`
            (${dbConstants.indicators.role.CLIENT_ROLE}, ${dbConstants.indicators.user.USER_LIST_OWN})
        `);

        ClientRoleAccessAndPermissionsQueries.push(`
            (${dbConstants.indicators.role.CLIENT_ROLE}, ${dbConstants.indicators.user.USER_EDIT_OWN})
        `);

        ClientRoleAccessAndPermissionsQueries.push(`
            (${dbConstants.indicators.role.CLIENT_ROLE}, ${dbConstants.indicators.user.USER_DEACTIVATE_OWN})
        `);

        ClientRoleAccessAndPermissionsQueries.push(`
            (${dbConstants.indicators.role.CLIENT_ROLE}, ${dbConstants.indicators.spending.SPENDING_ACCESS})
        `);

        ClientRoleAccessAndPermissionsQueries.push(`
            (${dbConstants.indicators.role.CLIENT_ROLE}, ${dbConstants.indicators.spending.SPENDING_CREATE})
        `);

        ClientRoleAccessAndPermissionsQueries.push(`
            (${dbConstants.indicators.role.CLIENT_ROLE}, ${dbConstants.indicators.spending.SPENDING_LIST_OWN})
        `);

        ClientRoleAccessAndPermissionsQueries.push(`
            (${dbConstants.indicators.role.CLIENT_ROLE}, ${dbConstants.indicators.spending.SPENDING_EDIT_OWN})
        `);

        ClientRoleAccessAndPermissionsQueries.push(`
            (${dbConstants.indicators.role.CLIENT_ROLE}, ${dbConstants.indicators.spending.SPENDING_DELETE_OWN})
        `);

        await this._connectionRef.query(`
            INSERT IGNORE INTO
                roles_access_permissions_entity (roleEntityId, accessPermissionsEntityId)
            VALUES
                ${ClientRoleAccessAndPermissionsQueries.join(',')}
        `);

    }
}

module.exports = { RoleSeed }