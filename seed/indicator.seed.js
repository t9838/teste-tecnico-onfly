const AccessSeed = require('./access.seed').AccessSeed;
const PermissionsSeed = require('./permissions.seed').PermissionsSeed;
const RoleSeed = require('./role.seed').RoleSeed;

class IndicatorSeed {

    _connectionRef;

    constructor(connection){
        this._connectionRef = connection;
    }

    execIndicatorSeeds = async () => {
        await new AccessSeed(this._connectionRef).execAccessSeeds();
        await new PermissionsSeed(this._connectionRef).execAPermissionsSeeds();
        await new RoleSeed(this._connectionRef).execRoleSeeds();
    }
}

module.exports = { IndicatorSeed }