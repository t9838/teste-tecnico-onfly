const dbConstants = require('../db.constants');

class IndicatorTypeSeed {

    _connectionRef;

    constructor(connection){
        this._connectionRef = connection;
    }

    execIndicatorTypeSeeds = async () => {
        await this._createRoleType();
        await this._createAccessType();
        await this._createPermissionType();
    }
    
    _createRoleType = async () => {
        await this._connectionRef.query(`
            INSERT IGNORE INTO indicator_type_entity (id, type, description, isEnum, active)
            VALUES (${dbConstants.indicatorsTypes.ROLE}, 'Role', 'Defines a Role', 1, 1)
        `)
    }

    _createAccessType = async () => {
        await this._connectionRef.query(`
            INSERT IGNORE INTO indicator_type_entity (id, type, description, isEnum, active)
            VALUES (${dbConstants.indicatorsTypes.ACCESS}, 'Access', 'Defines an Access', 1, 1)
        `)
    }

    _createPermissionType = async () => {
        await this._connectionRef.query(`
            INSERT IGNORE INTO indicator_type_entity (id, type, description, isEnum, active)
            VALUES (${dbConstants.indicatorsTypes.PERMISSION}, 'Permission', 'Defines a Permission', 1, 1)
        `)
    }

}

module.exports = { IndicatorTypeSeed }