const dbConstants = require('../db.constants');

class PermissionsSeed {

    _connectionRef;

    constructor(connection){
        this._connectionRef = connection;
    }

    execAPermissionsSeeds = async () => {
        await this._createUserPermissions();
        await this._createSpendingPermissions();
    }
    
    _createUserPermissions = async () => {

        const UserPermissionsQueries = [];

        UserPermissionsQueries.push(`
            (${dbConstants.indicators.user.USER_CREATE}, 'Create User permission', 'Can create a new User', 1, ${dbConstants.indicatorsTypes.PERMISSION})
        `);

        UserPermissionsQueries.push(`
            (${dbConstants.indicators.user.USER_LIST_ALL}, 'List all User permission', 'Can list any User', 1, ${dbConstants.indicatorsTypes.PERMISSION})
        `);

        UserPermissionsQueries.push(`
            (${dbConstants.indicators.user.USER_LIST_OWN}, 'List own User permission', 'Can list the current User', 1, ${dbConstants.indicatorsTypes.PERMISSION})
        `);

        UserPermissionsQueries.push(`
            (${dbConstants.indicators.user.USER_EDIT_ALL}, 'Edit User permission', 'Can edit any User', 1, ${dbConstants.indicatorsTypes.PERMISSION})
        `);

        UserPermissionsQueries.push(`
            (${dbConstants.indicators.user.USER_EDIT_OWN}, ' Edit own User permission', 'Can edit the current User', 1, ${dbConstants.indicatorsTypes.PERMISSION})
        `);

        UserPermissionsQueries.push(`
            (${dbConstants.indicators.user.USER_DEACTIVATE_ALL}, 'Deactivate all User permission', 'Can deactivate any User', 1, ${dbConstants.indicatorsTypes.PERMISSION})
        `);

        UserPermissionsQueries.push(`
            (${dbConstants.indicators.user.USER_DEACTIVATE_OWN}, 'Deactivate own User permission', 'Can deactivate the current User', 1, ${dbConstants.indicatorsTypes.PERMISSION})
        `);

        UserPermissionsQueries.push(`
            (${dbConstants.indicators.user.USER_DEACTIVATE_ALL}, 'Activate all User permission', 'Can activate any User', 1, ${dbConstants.indicatorsTypes.PERMISSION})
        `);

        await this._connectionRef.query(`
            INSERT IGNORE INTO 
                indicator_entity (id, value, description, active, indicatorTypeId)
            VALUES
                ${UserPermissionsQueries.join(',')}
        `);
    }

    _createSpendingPermissions = async () => {

        const SpendingPermissionsQueries = [];

        SpendingPermissionsQueries.push(`
            (${dbConstants.indicators.spending.SPENDING_CREATE}, 'Create Spending permission', 'Can create a new Spending', 1, ${dbConstants.indicatorsTypes.PERMISSION})
        `);

        SpendingPermissionsQueries.push(`
            (${dbConstants.indicators.spending.SPENDING_LIST_ALL}, 'List all Spending permission', 'Can list any Spending', 1, ${dbConstants.indicatorsTypes.PERMISSION})
        `);

        SpendingPermissionsQueries.push(`
            (${dbConstants.indicators.spending.SPENDING_LIST_OWN}, 'List own Spending permission', 'Can list the current Spending', 1, ${dbConstants.indicatorsTypes.PERMISSION})
        `);

        SpendingPermissionsQueries.push(`
            (${dbConstants.indicators.spending.SPENDING_EDIT_ALL}, 'Edit Spending permission', 'Can edit any Spending', 1, ${dbConstants.indicatorsTypes.PERMISSION})
        `);

        SpendingPermissionsQueries.push(`
            (${dbConstants.indicators.spending.SPENDING_EDIT_OWN}, ' Edit own Spending permission', 'Can edit the current Spending', 1, ${dbConstants.indicatorsTypes.PERMISSION})
        `);

        SpendingPermissionsQueries.push(`
            (${dbConstants.indicators.spending.SPENDING_DELETE_ALL}, 'Delete all Spending permission', 'Can delete any Spending', 1, ${dbConstants.indicatorsTypes.PERMISSION})
        `);

        SpendingPermissionsQueries.push(`
            (${dbConstants.indicators.spending.SPENDING_DELETE_OWN}, 'Delete own Spending permission', 'Can delete the current Spending', 1, ${dbConstants.indicatorsTypes.PERMISSION})
        `);

        await this._connectionRef.query(`
            INSERT IGNORE INTO 
                indicator_entity (id, value, description, active, indicatorTypeId)
            VALUES
                ${SpendingPermissionsQueries.join(',')}
        `);
    }

}

module.exports = { PermissionsSeed }