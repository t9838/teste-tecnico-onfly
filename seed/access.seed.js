const dbConstants = require('../db.constants');

class AccessSeed {

    _connectionRef;

    constructor(connection){
        this._connectionRef = connection;
    }

    execAccessSeeds = async () => {
        await this._createUserAccess();
        await this._createSpendingAccess();
    }
    
    _createUserAccess = async () => {
        await this._connectionRef.query(`
            INSERT IGNORE INTO indicator_entity (id, value, description, active, indicatorTypeId)
            VALUES (${dbConstants.indicators.user.USER_ACCESS}, 'User access', 'Can access the User module', 1, ${dbConstants.indicatorsTypes.ACCESS})
        `)
    }

    _createSpendingAccess = async () => {
        await this._connectionRef.query(`
            INSERT IGNORE INTO indicator_entity (id, value, description, active, indicatorTypeId)
            VALUES (${dbConstants.indicators.spending.SPENDING_ACCESS}, 'Spending access', 'Can access the Spending module', 1, ${dbConstants.indicatorsTypes.ACCESS})
        `)
    }
}

module.exports = { AccessSeed }