import {MigrationInterface, QueryRunner} from "typeorm";

export class customMigration1651112811102 implements MigrationInterface {
    name = 'initialMigration1651112811102'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`user_entity\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(255) NOT NULL, \`email\` varchar(255) NOT NULL, \`password\` varchar(255) NOT NULL, \`active\` tinyint NOT NULL DEFAULT 1, UNIQUE INDEX \`IDX_415c35b9b3b6fe45a3b065030f\` (\`email\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`indicator_entity\` (\`id\` int NOT NULL, \`value\` varchar(255) NOT NULL, \`description\` varchar(255) NOT NULL, \`active\` tinyint NOT NULL DEFAULT 1, \`indicatorTypeId\` int NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`indicator_type_entity\` (\`id\` int NOT NULL, \`type\` varchar(255) NOT NULL, \`description\` varchar(255) NOT NULL, \`isEnum\` tinyint NOT NULL DEFAULT 1, \`active\` tinyint NOT NULL DEFAULT 1, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`users_roles_entity\` (\`userEntityId\` int NOT NULL, \`roleEntityId\` int NOT NULL, INDEX \`IDX_0d60521e1cecf4bd669ebc8538\` (\`userEntityId\`), INDEX \`IDX_a2f84354ee39afb9dba12c21a8\` (\`roleEntityId\`), PRIMARY KEY (\`userEntityId\`, \`roleEntityId\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`roles_access_permissions_entity\` (\`roleEntityId\` int NOT NULL, \`accessPermissionsEntityId\` int NOT NULL, INDEX \`IDX_ccc7ecbfd7c3535f8166563783\` (\`roleEntityId\`), INDEX \`IDX_6622cb4bacf2a72b1db7c07776\` (\`accessPermissionsEntityId\`), PRIMARY KEY (\`roleEntityId\`, \`accessPermissionsEntityId\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`indicator_entity\` ADD CONSTRAINT \`FK_cf152aa75dd8de36bd88ae5448c\` FOREIGN KEY (\`indicatorTypeId\`) REFERENCES \`indicator_type_entity\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`users_roles_entity\` ADD CONSTRAINT \`FK_0d60521e1cecf4bd669ebc85389\` FOREIGN KEY (\`userEntityId\`) REFERENCES \`user_entity\`(\`id\`) ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE \`users_roles_entity\` ADD CONSTRAINT \`FK_a2f84354ee39afb9dba12c21a8a\` FOREIGN KEY (\`roleEntityId\`) REFERENCES \`indicator_entity\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`roles_access_permissions_entity\` ADD CONSTRAINT \`FK_ccc7ecbfd7c3535f81665637835\` FOREIGN KEY (\`roleEntityId\`) REFERENCES \`indicator_entity\`(\`id\`) ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE \`roles_access_permissions_entity\` ADD CONSTRAINT \`FK_6622cb4bacf2a72b1db7c077763\` FOREIGN KEY (\`accessPermissionsEntityId\`) REFERENCES \`indicator_entity\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`roles_access_permissions_entity\` DROP FOREIGN KEY \`FK_6622cb4bacf2a72b1db7c077763\``);
        await queryRunner.query(`ALTER TABLE \`roles_access_permissions_entity\` DROP FOREIGN KEY \`FK_ccc7ecbfd7c3535f81665637835\``);
        await queryRunner.query(`ALTER TABLE \`users_roles_entity\` DROP FOREIGN KEY \`FK_a2f84354ee39afb9dba12c21a8a\``);
        await queryRunner.query(`ALTER TABLE \`users_roles_entity\` DROP FOREIGN KEY \`FK_0d60521e1cecf4bd669ebc85389\``);
        await queryRunner.query(`ALTER TABLE \`indicator_entity\` DROP FOREIGN KEY \`FK_cf152aa75dd8de36bd88ae5448c\``);
        await queryRunner.query(`DROP INDEX \`IDX_6622cb4bacf2a72b1db7c07776\` ON \`roles_access_permissions_entity\``);
        await queryRunner.query(`DROP INDEX \`IDX_ccc7ecbfd7c3535f8166563783\` ON \`roles_access_permissions_entity\``);
        await queryRunner.query(`DROP TABLE \`roles_access_permissions_entity\``);
        await queryRunner.query(`DROP INDEX \`IDX_a2f84354ee39afb9dba12c21a8\` ON \`users_roles_entity\``);
        await queryRunner.query(`DROP INDEX \`IDX_0d60521e1cecf4bd669ebc8538\` ON \`users_roles_entity\``);
        await queryRunner.query(`DROP TABLE \`users_roles_entity\``);
        await queryRunner.query(`DROP TABLE \`indicator_type_entity\``);
        await queryRunner.query(`DROP TABLE \`indicator_entity\``);
        await queryRunner.query(`DROP INDEX \`IDX_415c35b9b3b6fe45a3b065030f\` ON \`user_entity\``);
        await queryRunner.query(`DROP TABLE \`user_entity\``);
    }

}
