import {MigrationInterface, QueryRunner} from "typeorm";

export class customMigration1651120813279 implements MigrationInterface {
    name = 'createSpendingEntity1651120813279'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`spending_entity\` (\`id\` int NOT NULL AUTO_INCREMENT, \`description\` varchar(191) NOT NULL, \`date\` datetime NOT NULL, \`value\` int NOT NULL, \`userId\` int NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`spending_entity\` ADD CONSTRAINT \`FK_c4e44b0fb3b9d22e1a04f84bda3\` FOREIGN KEY (\`userId\`) REFERENCES \`user_entity\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`spending_entity\` DROP FOREIGN KEY \`FK_c4e44b0fb3b9d22e1a04f84bda3\``);
        await queryRunner.query(`DROP TABLE \`spending_entity\``);
    }

}
