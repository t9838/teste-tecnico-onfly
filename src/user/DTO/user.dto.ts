import { ApiHideProperty } from '@nestjs/swagger';

import { Exclude, Type } from 'class-transformer';
import {
    IsEmail,
    IsNotEmpty,
    IsString,
    IsBoolean,
    IsNumber,
    IsObject,
    ValidateNested,
    IsOptional,
    IsNumberString,
} from 'class-validator';

import { RoleDTO } from '../../indicator';
import { SpendingDTO } from '../../spending';
export class UserDTO{
    
    @IsNotEmpty()
    @IsNumber()
    id: number;

    @IsNotEmpty()
    @IsString()
    name: string;
  
    @IsNotEmpty()
    @IsEmail()
    email: string;
  
    @Exclude()
    password: string;
  
    @IsNotEmpty()
    @IsBoolean()
    active: boolean;

    @ApiHideProperty()
    @IsOptional()
    @IsObject()
    @ValidateNested()
    @Type(() => RoleDTO)
    roles ?: Promise<RoleDTO[]>;

    @ApiHideProperty()
    @IsOptional()
    @IsObject()
    @ValidateNested()
    @Type(() => RoleDTO)
    __roles__ ?: RoleDTO[];

    @ApiHideProperty()
    @IsOptional()
    @IsObject()
    @ValidateNested()
    @Type(() => RoleDTO)
    spendings ?: Promise<SpendingDTO[]>;

    @ApiHideProperty()
    @IsOptional()
    @IsObject()
    @ValidateNested()
    @Type(() => RoleDTO)
    __spendings__ ?: SpendingDTO[];
  
}

export class CreateUserDTO{
    
    @IsNotEmpty()
    @IsString()
    name: string;
  
    @IsNotEmpty()
    @IsEmail()
    email: string;
  
    @IsNotEmpty()
    @IsString()
    password: string;
}

export class UpdateUserDTO{

    @IsOptional()
    @IsString()
    name: string;
  
    @IsOptional()
    @IsEmail()
    email: string;

    @IsOptional()
    @IsString()
    password: string;
}

export class UserParamsDTO{
    
    @IsNotEmpty()
    @IsNumberString()
    id ?: number;
}