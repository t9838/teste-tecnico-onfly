import {
    Body,
    Controller,
    Get,
    HttpException,
    HttpStatus,
    Param,
    Post,
    Put,
    UseGuards
} from '@nestjs/common';
import { UserService } from '../Service';
import {
    UserDTO,
    CreateUserDTO,
    UpdateUserDTO,
    UserParamsDTO
} from '../DTO';
import {
    JwtAuthGuard,
    RolesGuard,
    BelongsToUserGuard,
    ClaimGuard,
    Roles,
    BelongsToUser,
    Claim,
    RolesEnum,
    BelongsToUserSearchOnEnum
} from '../../auth';
import { FeedbackResponse } from '../../utils';
import {
    ApiBearerAuth,
    ApiTags
} from '@nestjs/swagger';
import * as dbConstants from 'db.constants.js';

@ApiBearerAuth()
@ApiTags('Users')
@Controller('user')
export class UserController {

    constructor(
        private readonly userService: UserService
    ){}

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles(RolesEnum.ADMIN)
    @Post()
    async createNewUser(@Body() userToCreate: CreateUserDTO): Promise<UserDTO>{
        try{
            const Response = await this.userService.createNewUser(userToCreate);
            return Response
        }
        catch(err){
            throw err
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles(RolesEnum.ADMIN)
    @Get()
    async getAllUsers(): Promise<UserDTO[]>{
        try{
            const Response = await this.userService.getAllUsers();
            return Response
        }
        catch(err){
            throw err
        }
    }

    @UseGuards(JwtAuthGuard, ClaimGuard, BelongsToUserGuard)
    @Claim({
        claimId: dbConstants.indicators.user.USER_LIST_OWN
    })
    @BelongsToUser({
        searchOn: BelongsToUserSearchOnEnum.URL_PARAMS,
        searchParam: 'id',
        globalActionPermissionId: dbConstants.indicators.user.USER_LIST_ALL
    })
    @Get(':id')
    async getUserById(@Param() userParams: UserParamsDTO): Promise<UserDTO>{
        try{
            const Response = await this.userService.getUserById(userParams.id);
            return Response
        }
        catch(err){
            throw err
        }
    }

    @UseGuards(JwtAuthGuard, ClaimGuard, BelongsToUserGuard)
    @Claim({
        claimId: dbConstants.indicators.user.USER_EDIT_OWN
    })
    @BelongsToUser({
        searchOn: BelongsToUserSearchOnEnum.URL_PARAMS,
        searchParam: 'id',
        globalActionPermissionId: dbConstants.indicators.user.USER_EDIT_ALL
    })
    @Put(':id')
    async updateUserById(@Param() userParams: UserParamsDTO, @Body() userToUpdate: UpdateUserDTO): Promise<FeedbackResponse>{
        try{
            if(!Object.keys(userToUpdate).length){
                throw new HttpException('Nada para atualizar', HttpStatus.BAD_REQUEST)
            }
            const Response = await this.userService.updateUserByid(userParams.id, userToUpdate);
            return Response
        }
        catch(err){
            throw err
        }
    }

    @UseGuards(JwtAuthGuard, ClaimGuard, BelongsToUserGuard)
    @Claim({
        claimId: dbConstants.indicators.user.USER_DEACTIVATE_OWN
    })
    @BelongsToUser({
        searchOn: BelongsToUserSearchOnEnum.URL_PARAMS,
        searchParam: 'id',
        globalActionPermissionId: dbConstants.indicators.user.USER_DEACTIVATE_ALL
    })
    @Put('deactivate/:id')
    async deactivateUserById(@Param() userParams: UserParamsDTO): Promise<FeedbackResponse>{
        try{
            const Response = await this.userService.deactivateUserById(userParams.id);
            return Response
        }
        catch(err){
            throw err
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles(RolesEnum.ADMIN)
    @Put('activate/:id')
    async activateUserById(@Param() userParams: UserParamsDTO): Promise<FeedbackResponse>{
        try{
            const Response = await this.userService.activateteUserById(userParams.id);
            return Response
        }
        catch(err){
            throw err
        }
    }

}