import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserController } from '../Controller';
import { UserEntity, UserEntitySubscriber } from '../Entity';
import { UserService } from '../Service';
import { IndicatorModule } from '../../indicator';

@Module({
    imports: [
      TypeOrmModule.forFeature([UserEntity]),
      IndicatorModule
    ],
    controllers: [ UserController ],
    providers: [ UserEntitySubscriber, UserService ],
    exports: [ UserService ]
  })
export class UserModule {}
