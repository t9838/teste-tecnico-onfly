import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    EventSubscriber,
    EntitySubscriberInterface,
    Connection,
    InsertEvent,
    UpdateEvent,
    ManyToMany,
    JoinTable,
    OneToMany
} from 'typeorm';

import { IndicatorEntity } from '../../indicator/Entities/indicators.entity';
import { SpendingEntity } from '../../spending/Entity/spending.entity';

import { HashPassword } from '../../utils/Services'

@Entity()
export class UserEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ unique: true })
  email: string;

  @Column({ select: false })
  password: string;

  @Column({ default: true })
  active: boolean;

  @ManyToMany(() => IndicatorEntity, indicator => indicator.users)
  @JoinTable({
    name: 'users_roles_entity',
    inverseJoinColumn: {
      name: 'roleEntityId',
    },
  })
  roles: Promise<IndicatorEntity[]>;

  @OneToMany(() => SpendingEntity, spending => spending.user)
  spendings: Promise<SpendingEntity[]>;
}

@EventSubscriber()
export class UserEntitySubscriber implements EntitySubscriberInterface<UserEntity> {
  constructor(connection: Connection) {
    connection.subscribers.push(this);
  }

  listenTo() {
    return UserEntity;
  }

  async beforeInsert(event: InsertEvent<UserEntity>) {
    event.entity['password'] = await HashPassword(event.entity['password'])
  }

  async beforeUpdate(event: UpdateEvent<UserEntity>) {
    if(event.entity['password']){
      event.entity['password'] = await HashPassword(event.entity['password'])
    }
  }
}