export enum ActionsEnum{
    CREATE = 'Create',
    LIST = 'List',
    UPDATE = 'Update',
    DELETE = 'Delete',
    DEACTIVATE = 'Deactivate',
    ACTIVATE = 'Activate'
}