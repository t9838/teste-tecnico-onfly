import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MailService, MailDataRequired } from '@sendgrid/mail'; 
import { SpendingDTO } from '../../spending';

@Injectable()
export class SendGridService {

    private mailService!: MailService;

    constructor(
        private readonly configService: ConfigService
    ){
        this.initService();
    }

    private initService(){
        this.mailService = new MailService();
        this.mailService.setApiKey(this.configService.get<string>('SENDGRID_API_KEY'));
    }



    public async sendEmail(spending: SpendingDTO): Promise<boolean>{
        try{
            const Data: MailDataRequired = {
                to: spending.user.email,
                from: this.configService.get<string>('SENDGRID_SENDER'),
                subject: 'Despesa Cadastrada',
                html: `
                    A Despesa <strong>${spending.description}</strong>
                    no valor de <strong>R$ ${spending.value}</strong>
                    do dia <strong>R$ ${spending.date.toDateString()}</strong>
                    foi cadastrada com sucesso!`
            }
            const Response = await this.sendEmailAPICall(Data);
            return Response
        }
        catch(err){
            if(err instanceof HttpException){
                throw err
            }

            throw new HttpException(err.message, HttpStatus.BAD_REQUEST) 
        }
    }

    private async sendEmailAPICall(data:MailDataRequired ): Promise<boolean>{

        const response = await this.mailService.send(data).then(res => {
            return true
        }).catch((err) => {
            throw new HttpException(err.response.body.errors[0].message, err.code)
        })

        return response
    }


}