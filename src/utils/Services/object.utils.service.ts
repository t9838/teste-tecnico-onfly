import { HttpException, HttpStatus } from "@nestjs/common";
import { RolesEnum } from '../../auth/Enums';

export const FixLazyLoadingProps = (target: Object): Object => {
    Object.keys(target).forEach(key => {
        if(key.substring(0,2) === '__' && key.includes('has')){
            delete target[key];
        }
    })

    return target
}

export const RemoveDuplicatesFromObjectArray = (targetArray: any[]): any[] => {

    return targetArray.filter((item, index ,array)=> {
        return array.findIndex(nestedItem => {
            return nestedItem.id === item.id
        }) === index
    })

}

export const CheckAccessPermissionOnObject = (objectOwner, req): void => {
    const CurrentUserId = req.user.id;
    const IsAdmin = req.roles.filter(userRole => userRole.id === RolesEnum.ADMIN && userRole.active ).length ? true : false;

    if(objectOwner !== CurrentUserId && !IsAdmin){
        throw new HttpException('Permissões insuficientes', HttpStatus.FORBIDDEN) 
    }
}