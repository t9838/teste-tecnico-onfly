import {
    Entity,
    Column,
    PrimaryColumn,
    ManyToOne,
    ManyToMany,
    JoinTable,
    OneToMany
  } from 'typeorm';
  
  import { IndicatorTypeEntity } from './indicatorType.entity';
  import { UserEntity } from '../../user/Entity/user.entity';

  @Entity()
  export class IndicatorEntity {
    @PrimaryColumn()
    id: number;

    @Column()
    value: string;
  
    @Column()
    description: string;
  
    @Column({ default: true })
    active: boolean;

    @ManyToOne(() => IndicatorTypeEntity, indicatorType => indicatorType.indicators)
    indicatorType: Promise<IndicatorTypeEntity>;

    @ManyToMany(() => IndicatorEntity, indicator => indicator.accessPermissions)
    @JoinTable({ 
      name: 'roles_access_permissions_entity',
      joinColumn: {
        name: 'roleEntityId',
      },
      inverseJoinColumn: {
        name: 'accessPermissionsEntityId',
      },
    })
    accessPermissions: Promise<IndicatorEntity[]>;

    @ManyToMany(() => UserEntity, user => user.roles)
    users: Promise<UserEntity[]>

  }