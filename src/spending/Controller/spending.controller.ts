import {
    Body,
    Controller,
    Delete,
    Get,
    HttpException,
    HttpStatus,
    Param,
    Post,
    Put,
    Request,
    UseGuards
} from '@nestjs/common';
import { SpendingService } from '../Service';
import {
    SpendingDTO,
    CreateSpendingDTO,
    UpdateSpendingDTO,
    SpendingParamsDTO
} from '../DTO';
import {
    JwtAuthGuard,
    RolesGuard,
    BelongsToUserGuard,
    ClaimGuard,
    Roles,
    BelongsToUser,
    Claim,
    RolesEnum,
    BelongsToUserSearchOnEnum
} from '../../auth';
import { FeedbackResponse } from '../../utils';
import {
    ApiBearerAuth,
    ApiTags
} from '@nestjs/swagger';
import * as dbConstants from 'db.constants.js';

@ApiBearerAuth()
@ApiTags('spending')
@Controller('spending')
export class SpendingController {

    constructor(
        private readonly SpendingService: SpendingService
    ){}

    @UseGuards(JwtAuthGuard, ClaimGuard)
    @Claim({
        claimId: dbConstants.indicators.spending.SPENDING_CREATE
    })
    @Post()
    async createNewUser(@Body() spendingToCreate: CreateSpendingDTO, @Request() req): Promise<SpendingDTO>{
        try{
            const Response = await this.SpendingService.createNewSpending(spendingToCreate, req.user);
            return Response
        }
        catch(err){
            throw err
        }
    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles(RolesEnum.ADMIN)
    @Get()
    async getAllSpendings(): Promise<SpendingDTO[]>{
        try{
            const Response = await this.SpendingService.getAllSpendings();
            return Response
        }
        catch(err){
            throw err
        }
    }

    @UseGuards(JwtAuthGuard, ClaimGuard, BelongsToUserGuard)
    @Claim({
        claimId: dbConstants.indicators.spending.SPENDING_LIST_OWN
    })
    @BelongsToUser({
        searchOn: BelongsToUserSearchOnEnum.URL_PARAMS,
        searchParam: 'id',
        globalActionPermissionId: dbConstants.indicators.spending.SPENDING_LIST_ALL
    })
    @Get('user/:id')
    async getAllUserSpendings(@Param() spendingParams: SpendingParamsDTO): Promise<SpendingDTO[]>{
        try{
            const Response = await this.SpendingService.getAllUserSpendings(spendingParams.id);
            return Response
        }
        catch(err){
            throw err
        }
    }

    @UseGuards(JwtAuthGuard, ClaimGuard)
    @Claim(
        {
            claimId: dbConstants.indicators.spending.SPENDING_LIST_ALL
        },
        {
            claimId: dbConstants.indicators.spending.SPENDING_LIST_OWN
        }
    )
    @Get(':id')
    async getSpendingById(@Param() spendingParams: SpendingParamsDTO, @Request() req): Promise<SpendingDTO>{
        try{
            const Response = await this.SpendingService.getSpendingById(spendingParams.id, req);
            return Response
        }
        catch(err){
            throw err
        }
    }

    @UseGuards(JwtAuthGuard, ClaimGuard)
    @Claim(
        {
            claimId: dbConstants.indicators.spending.SPENDING_EDIT_ALL
        },
        {
            claimId: dbConstants.indicators.spending.SPENDING_EDIT_OWN
        },
    )
    @Put(':id')
    async updateSpendingById(@Param() spendingParams: SpendingParamsDTO, @Body() spendingToUpdate: UpdateSpendingDTO, @Request() req): Promise<FeedbackResponse>{
        try{
            if(!Object.keys(spendingToUpdate).length){
                throw new HttpException('Nada para atualizar', HttpStatus.BAD_REQUEST)
            }
            const Response = await this.SpendingService.updateSpendingByid(spendingParams.id, spendingToUpdate, req);
            return Response
        }
        catch(err){
            throw err
        }
    }

    @UseGuards(JwtAuthGuard, ClaimGuard)
    @Claim(
        {
            claimId: dbConstants.indicators.spending.SPENDING_DELETE_ALL
        },
        {
            claimId: dbConstants.indicators.spending.SPENDING_DELETE_OWN
        }
    )
    @Delete(':id')
    async deleteSpendingById(@Param() spendingParams: SpendingParamsDTO, @Request() req): Promise<FeedbackResponse>{
        try{
            const Response = await this.SpendingService.deleteSpendingById(spendingParams.id, req);
            return Response
        }
        catch(err){
            throw err
        }
    }

}
