import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToOne
} from 'typeorm';

import { UserEntity } from '../../user/Entity/user.entity';

@Entity()
export class SpendingEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 191 })
  description: string;

  @Column()
  date: Date;

  @Column()
  value: number;

  @ManyToOne(() => UserEntity, user => user.spendings, {
    eager: true
  })
  user: UserEntity;

}