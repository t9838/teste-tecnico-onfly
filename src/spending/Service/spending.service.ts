import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SpendingEntity } from '../Entity';
import {
    CreateSpendingDTO,
    SpendingDTO,
    UpdateSpendingDTO
} from '../DTO';
import { UserDTO, UserEntity } from '../../user';
import {
    FeedbackResponse,
    ActionsEnum,
    StatusEnum, 
    CheckAccessPermissionOnObject,
    SendGridService
} from '../../utils';

@Injectable()
export class SpendingService {

    constructor(
        @InjectRepository(SpendingEntity)
        private readonly spendingRepository: Repository<SpendingEntity>,

        private readonly sendGridService: SendGridService
    ) {}

    public async createNewSpending(spendingToCreate: CreateSpendingDTO, currentUser: UserDTO): Promise<SpendingDTO>{
        try{

            const NewSpendingEntity = new SpendingEntity();

            Object.keys(spendingToCreate).forEach(key => {
                NewSpendingEntity[key] = spendingToCreate[key]
            });

            NewSpendingEntity.user = currentUser as UserEntity;
            
            const NewSpending = await this.spendingRepository.save(NewSpendingEntity);

            await this.sendGridService.sendEmail(NewSpending);

            return NewSpending
        }
        catch(err){
            throw err
        }
    }
    
    public async getAllSpendings(): Promise<SpendingDTO[]>{
        try{
            const SpendingsList = await this.spendingRepository.find();
            return SpendingsList 
        }
        catch(err){
            throw err
        }
    }

    public async getAllUserSpendings(userId: number): Promise<SpendingDTO[]>{
        try{
            const SpendingsList = await this.spendingRepository.find({
                where: {
                    user: {
                        id: userId
                    }
                },
                relations: [
                    'user'
                ]
            });
            return SpendingsList 
        }
        catch(err){
            throw err
        }
    }

    public async getSpendingById(spendingId: number, req: any): Promise<SpendingDTO>{
        try{
            const FoundedSpending = await this.spendingRepository.findOne({
                where: {
                    id: spendingId
                }       
            });

            if(!FoundedSpending){
                throw new HttpException('Despesa não encontrada', HttpStatus.NOT_FOUND)
            }

            const SpendingUser = await FoundedSpending.user;

            CheckAccessPermissionOnObject(SpendingUser.id, req);
            
            return FoundedSpending 
        }
        catch(err){
            throw err
        }
    }

    public async updateSpendingByid(spendingId: number, spendingToUpdate: UpdateSpendingDTO, req: any): Promise<FeedbackResponse>{
        try{
            const FoundedSpending = await this.getSpendingById(spendingId, req);
            await this.spendingRepository.update(spendingId, spendingToUpdate);
            const UpdateResponse: FeedbackResponse = {
                action: ActionsEnum.UPDATE,
                status: StatusEnum.SUCCESS,
                message: `Despesa '${spendingToUpdate.description || FoundedSpending.description}' alterada com sucesso`
            }
            return UpdateResponse
        }
        catch(err){

            const UpdateResponse: FeedbackResponse = {
                action: ActionsEnum.UPDATE,
                status: StatusEnum.ERROR,
                message: err.message
            }

            if(err instanceof HttpException){
                throw new HttpException(UpdateResponse, err.getStatus())
            }

            throw new HttpException(UpdateResponse, HttpStatus.BAD_REQUEST)
        }
    }

    public async deleteSpendingById(spendingId: number, req: any): Promise<FeedbackResponse>{
        try{
            const FoundedSpending = await this.getSpendingById(spendingId, req);

            await this.spendingRepository.delete(spendingId);
            const DeleteResponse: FeedbackResponse = {
                action: ActionsEnum.DELETE,
                status: StatusEnum.SUCCESS,
                message: `Despesa '${FoundedSpending.description}' deletada com sucesso`
            }

            return DeleteResponse
 
        }
        catch(err){
            const DeactivateResponse: FeedbackResponse = {
                action: ActionsEnum.DELETE,
                status: StatusEnum.ERROR,
                message: err.message
            }

            if(err instanceof HttpException){
                throw new HttpException(DeactivateResponse, err.getStatus())
            }

            throw new HttpException(DeactivateResponse, HttpStatus.BAD_REQUEST)        
        }
    }

}
