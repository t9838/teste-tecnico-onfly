import { ApiHideProperty } from '@nestjs/swagger';

import { Type } from 'class-transformer';
import {
    IsNotEmpty,
    IsString,
    IsNumber,
    IsObject,
    ValidateNested,
    IsOptional,
    IsNumberString,
    IsDate,
    MaxLength,
    Min,
    MaxDate
} from 'class-validator';

import { UserDTO } from '../../user';

export class SpendingDTO{
    
    @IsNotEmpty()
    @IsNumber()
    id: number;

    @IsNotEmpty()
    @IsString()
    @MaxLength(191)
    description: string;
  
    @IsNotEmpty()
    @IsDate()
    @Type(() => Date)
    @MaxDate(new Date())
    date: Date;
  
    @IsNotEmpty()
    @IsNumber()
    @Min(0)
    value: number;

    @IsOptional()
    @IsObject()
    @ValidateNested()
    @Type(() => UserDTO)
    user : UserDTO

}

export class CreateSpendingDTO{
    
    @IsNotEmpty()
    @IsString()
    @MaxLength(191)
    description: string;
  
    @IsNotEmpty()
    @IsDate()
    @Type(() => Date)
    @MaxDate(new Date())
    date: Date;
  
    @IsNotEmpty()
    @IsNumber()
    @Min(0)
    value: number;
}

export class UpdateSpendingDTO{

    @IsOptional()
    @IsString()
    @MaxLength(191)
    description: string;
  
    @IsOptional()
    @IsDate()
    @Type(() => Date)
    @MaxDate(new Date())
    date: Date;
  
    @IsOptional()
    @IsNumber()
    @Min(0)
    value: number;
}

export class SpendingParamsDTO{
    
    @IsNotEmpty()
    @IsNumberString()
    id ?: number;
}