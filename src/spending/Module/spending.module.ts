import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SpendingController } from '../Controller';
import { SpendingService } from '../Service';
import { SpendingEntity } from '../Entity';
import { SendGridService } from '../../utils';

@Module({
  imports: [
    TypeOrmModule.forFeature([SpendingEntity]),
  ],
  providers: [ SpendingService, SendGridService ],
  controllers: [ SpendingController ]
})
export class SpendingModule {}
