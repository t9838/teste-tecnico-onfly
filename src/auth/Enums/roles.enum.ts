import * as dbConstants from 'db.constants.js'

export enum RolesEnum {
    ADMIN = dbConstants.indicators.role.ADMIN_ROLE,
    CLIENT = dbConstants.indicators.role.CLIENT_ROLE
}