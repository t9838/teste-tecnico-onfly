import { Injectable, CanActivate, ExecutionContext, HttpStatus } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { BELONGS_TO_USER__KEY } from '../Decorators';
import { BelongsToUserDTO } from '../DTO';
import { GenerateDefaultErrorResponse } from '../../utils';
import { UserDTO } from '../../user';
import { IndicatorDTO } from '../../indicator';

@Injectable()
export class BelongsToUserGuard implements CanActivate {

    constructor(private reflector: Reflector){}

    canActivate(context: ExecutionContext): boolean {

        const searchCriteria = this.reflector.getAllAndOverride<BelongsToUserDTO[]>(BELONGS_TO_USER__KEY, [
            context.getHandler(),
            context.getClass(),
        ]);

        if (!searchCriteria) {
            return true;
        }

        const CurrentUser = context.switchToHttp().getRequest().user as UserDTO;
        const CurrentAccessPermissions = context.switchToHttp().getRequest().accessPermissions as IndicatorDTO[];


        const hasClaim = searchCriteria.some((search) => {

            const MatchedClaim = CurrentAccessPermissions.filter(accessPermission => accessPermission.id === search?.globalActionPermissionId && accessPermission.active);

            if(!MatchedClaim.length){
                return false
            }

            return true
        });

        if(hasClaim){
            return true
        }

        if(!searchCriteria){
            throw GenerateDefaultErrorResponse(HttpStatus.FORBIDDEN, { message: 'Permissões insuficientes'})
        }

        const BelongToUser = searchCriteria.some((search) => {
            const SearchTarget = context.switchToHttp().getRequest()[search.searchOn];

            if(`${CurrentUser.id}` !== `${SearchTarget[search.searchParam]}`){
                return false
            }

            return true
        });

        if(!BelongToUser){
            throw GenerateDefaultErrorResponse(HttpStatus.FORBIDDEN, { message: 'Permissões insuficientes'})
        }

        return BelongToUser
    }
}