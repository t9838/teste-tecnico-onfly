import { Injectable, CanActivate, ExecutionContext, HttpStatus } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { CLAIM__KEY } from '../Decorators';
import { ClaimDTO } from '../DTO';
import { GenerateDefaultErrorResponse } from '../../utils';
import { IndicatorDTO } from '../../indicator';

@Injectable()
export class ClaimGuard implements CanActivate {

    constructor(private reflector: Reflector){}

    canActivate(context: ExecutionContext): boolean {

        const requiredAccessPermission = this.reflector.getAllAndOverride<ClaimDTO[]>(CLAIM__KEY, [
            context.getHandler(),
            context.getClass(),
        ]);

        if (!requiredAccessPermission) {
            return true;
        }

        const CurrentAccessPermissions = context.switchToHttp().getRequest().accessPermissions as IndicatorDTO[];


        const hasClaim = requiredAccessPermission.some((claim) => {

            const MatchedClaim = CurrentAccessPermissions.filter(accessPermission => accessPermission.id === claim.claimId && accessPermission.active);

            if(!MatchedClaim.length){
                return false
            }

            return true
        });

        if(!hasClaim){
            throw GenerateDefaultErrorResponse(HttpStatus.FORBIDDEN, { message: 'Permissões insuficientes'})
        }

        return hasClaim
    }
}