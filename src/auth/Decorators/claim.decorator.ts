import { SetMetadata } from '@nestjs/common';
import { ClaimDTO } from '../DTO';

export const CLAIM__KEY = 'claim';
export const Claim = ( ...claim: ClaimDTO[] ) => SetMetadata(CLAIM__KEY, claim);