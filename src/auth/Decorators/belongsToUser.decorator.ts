import { SetMetadata } from '@nestjs/common';
import { BelongsToUserDTO } from '../DTO';

export const BELONGS_TO_USER__KEY = 'belongsToUser';
export const BelongsToUser = (...belongsToUser: BelongsToUserDTO[]) => SetMetadata(BELONGS_TO_USER__KEY, belongsToUser);