import {
    IsNotEmpty,
    IsObject,
    IsNumber,
    IsString,
    IsDefined,
    IsNotEmptyObject,
    ValidateNested
} from 'class-validator';
import { Type } from 'class-transformer';
import { UserDTO } from '../../user';
import { IndicatorDTO } from '../../indicator';

export interface JWTPayloadDTO{
    user: UserDTO,
    roles: IndicatorDTO[],
    accessPermissions: IndicatorDTO[]
}

export class JWTLoginResponseDTO{
    
    @IsObject()
    @IsDefined()
    @IsNotEmptyObject()
    @ValidateNested()
    @Type(() => UserDTO)
    user: UserDTO;

    @IsObject()
    @IsDefined()
    @IsNotEmptyObject()
    @ValidateNested()
    @Type(() => IndicatorDTO)
    roles: IndicatorDTO[];

    @IsObject()
    @IsDefined()
    @IsNotEmptyObject()
    @ValidateNested()
    @Type(() => IndicatorDTO)
    accessPermissions: IndicatorDTO[];

    @IsNotEmpty()
    @IsString()
    authToken: string;

    @IsNotEmpty()
    @IsNumber()
    expiresIn: number;

}