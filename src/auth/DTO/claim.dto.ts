import {
    IsNotEmpty,
    IsNumber,
} from 'class-validator';

export class ClaimDTO{
    
    @IsNotEmpty()
    @IsNumber()
    claimId: number;

}