import {
    IsNotEmpty,
    IsNumber,
    IsString
} from 'class-validator';

export class BelongsToUserDTO{
    
    @IsNotEmpty()
    @IsString()
    searchOn: string;
  
    @IsNotEmpty()
    @IsString()
    searchParam: string;

    @IsNotEmpty()
    @IsNumber()
    globalActionPermissionId ?: number;
}