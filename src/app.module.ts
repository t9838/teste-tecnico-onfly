import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { InterceptorModule } from './interceptor';
import { UserModule } from './user';
import { AuthModule } from './auth';
import { IndicatorModule } from './indicator';
import { SpendingModule } from './spending';
import { config } from 'dotenv'

config();

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRoot({
      type: process.env.DB_MS as any,
      host: process.env.DB_HOST,
      port: Number(process.env.DB_PORT),
      username: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      autoLoadEntities: true,
      synchronize: JSON.parse(process.env.DB_SYNC),
      logging: JSON.parse(process.env.DB_LOG),
    }),
    InterceptorModule,
    UserModule,
    AuthModule,
    IndicatorModule,
    SpendingModule,
    SpendingModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
