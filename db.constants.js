module.exports = {
    //Indicators Types Id`s
    indicatorsTypes:{
        ROLE: 1,
        ACCESS: 2,
        PERMISSION: 3
    },
    indicators: {
        // Roles Id`s
        role: {
            ADMIN_ROLE: 50,
            CLIENT_ROLE: 51,
        },
        // User Module Access and Permissions
        user:{
            USER_ACCESS: 100,
            USER_CREATE: 200,
            USER_LIST_OWN: 201,
            USER_LIST_ALL: 202,
            USER_EDIT_OWN: 203,
            USER_EDIT_ALL: 204,
            USER_DEACTIVATE_ALL: 205,
            USER_DEACTIVATE_OWN: 206,
            USER_ACTIVATE_ALL: 205,
        },
        // Spending Module Access and Permissions
        spending:{
            SPENDING_ACCESS: 101,
            SPENDING_CREATE: 220,
            SPENDING_LIST_OWN: 221,
            SPENDING_LIST_ALL: 222,
            SPENDING_EDIT_OWN: 223,
            SPENDING_EDIT_ALL: 224,
            SPENDING_DELETE_ALL: 225,
            SPENDING_DELETE_OWN: 226,
        }
    }
}