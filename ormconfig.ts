import * as dotenv from 'dotenv';
dotenv.config();

export default {
  type: process.env.DB_MS,
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  entities: [
      "src/**/*entity.ts"
  ],
  migrations: ["migrations/*.ts"],
  cli: {
    migrationsDir: 'migrations'
  },
  logging: JSON.parse(process.env.DB_LOG),
}